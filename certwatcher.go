package cert2amqp

import (
	"io/ioutil"
	"log"
	"path/filepath"
	"time"

	"github.com/fsnotify/fsnotify"
)

// WatchCertDir watches path for new certificate files
func WatchCertDir(path string, errchan chan error, ch chan string) error {
	// create fsnotify watcher
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		errchan <- NewFatalError(err, "Unable to create fsnotify watcher")
		return err
	}
	err = watcher.Add(path)
	if err != nil {
		errchan <- NewFatalErrorf(err, "Unable to watch %s: %s", path, err)
		return err
	}

	// add new files on write
	go func() {
		for {
			select {
			case event := <-watcher.Events:
				if event.Op&fsnotify.Write == fsnotify.Write {
					//log.Printf("[WATCHER] Detected %s", event.Name)
					ch <- event.Name
				}
			case err := <-watcher.Errors:
				errchan <- NewNonFatalErrorf(err, "Error watching path %s", path)
			}
		}
	}()
	log.Printf("Watching %s for new certificates", path)

	// periodically read existing files
	go func() {
		for {
			files, err := ioutil.ReadDir(path)
			if err != nil {
				errchan <- NewFatalErrorf(err, "Unable to open directory %s: %s", path, err)
				return
			}
			for _, file := range files {
				if !file.IsDir() {
					//log.Printf("[STATIC] Found %s", file.Name())
					ch <- filepath.Join(path, file.Name())
				}
			}
			time.Sleep(time.Minute * 20)
		}
	}()

	return nil
}
