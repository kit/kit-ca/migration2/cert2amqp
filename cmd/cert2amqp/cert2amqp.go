//go:generate goversioninfo -icon=../../res/icon.ico

package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/url"
	"os"
	"os/exec"
	"os/user"
	"path/filepath"
	"runtime"

	"git.scc.kit.edu/KIT-CA/cert2amqp"
)

type Configuration struct {
	configfile    string
	ConnectString string `json:"connectstring"`
	Source        string `json:"source"`
	Exchange      string `json:"exchange"`
	Queue         string `json:"queue"`
	RemoveFile    bool   `json:"removefile"`
}

const (
	FILENAMECHANLEN = 32768
	CERTCHANLEN     = 32768
	DELETECHANLEN   = 32768
	ERRORCHANLEN    = 32768
)

var (
	Config = Configuration{
		ConnectString: "amqp://localhost:5672/",
		Source:        "Certificates",
		Exchange:      "newCertificates",
		Queue:         "ca2ad",
		RemoveFile:    true,
	}
	filenamechan = make(chan string, FILENAMECHANLEN)
	certChan     = make(chan *cert2amqp.Certificate, CERTCHANLEN)
	errorChan    = make(chan error, ERRORCHANLEN)
	deletechan   = make(chan string, DELETECHANLEN)
)

// CleanedConnectString removes password from connect string
func CleanedConnectString(uri string) string {
	u, err := url.Parse(uri)
	if err != nil {
		return ""
	}
	if u.User != nil {
		u.User = url.User(u.User.Username())
	}
	return u.String()
}

func init() {
	//log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetFlags(log.LstdFlags | log.Llongfile)
	// read configuration
	Config = ReadConfiguration("")
	// parse commandline
	flag.StringVar(&Config.ConnectString, "connect", Config.ConnectString, "AMQP connection string")
	flag.StringVar(&Config.Source, "source", Config.Source, "Certificate directory")
	flag.StringVar(&Config.Exchange, "exchange", Config.Exchange, "Destination exchange")
	flag.StringVar(&Config.Queue, "queue", Config.Queue, "Destination queue")
	flag.BoolVar(&Config.RemoveFile, "delete", Config.RemoveFile, "Remove source file after publication")
	flag.Parse()
	// start error processing
	cert2amqp.StartErrorHandler(errorChan)
	// print config
	log.Printf("[CONF] configuration file: %s", Config.configfile)
	log.Printf("[CONF] AMQP server:        %s", CleanedConnectString(Config.ConnectString))
	log.Printf("[CONF] source:             %s", Config.Source)
	log.Printf("[CONF] exchange:           %s", Config.Exchange)
	log.Printf("[CONF] queue:              %s", Config.Queue)
	log.Printf("[CONF] remove files:       %v", Config.RemoveFile)
}

// ReadConfiguration reads and parses configuration file
func ReadConfiguration(filename string) Configuration {
	var c Configuration
	// example/default config
	defaultConfig := Configuration{
		ConnectString: "amqps://user:pass@hostname/vhost",
		Source:        "CertificateSourceDirectory",
		Exchange:      "ExchangeName",
		Queue:         "QueueName",
		RemoveFile:    true,
	}

	// read ~/.config/cert2amqp.conf as default
	if filename == "" {
		username, err := user.Current()
		if err != nil {
			errorChan <- cert2amqp.NewFatalError(err, "Unable to get current username")
		}
		if runtime.GOOS == "windows" {
			filename = filepath.Join(username.HomeDir, "AppData", "Roaming", "cert2amqp", "cert2amqp.conf")
		} else {
			filename = filepath.Join(username.HomeDir, ".config", "cert2amqp.conf")
		}
	}

	file, err := os.Open(filename)
	// unable to open file
	if err != nil {
		log.Printf("Unable to open configuration file %s", filename)
		dir, _ := filepath.Split(filename)
		// path to config does not exist
		if _, err := os.Stat(dir); os.IsNotExist(err) {
			log.Printf("Config directory %s does not exist,", dir)
			err = os.MkdirAll(dir, 0600)
			if err != nil {
				log.Println("Unable to create config directory", err)
			} else {
				log.Printf("Config directory %s created", dir)
			}
		}
		// create default config file
		j, _ := json.MarshalIndent(defaultConfig, "", "    ")
		err = os.WriteFile(filename, j, 0700)
		if err != nil {
			errorChan <- cert2amqp.NewNonFatalErrorf(err, "Unable to create new default config file %s", filename)
		} else {
			errorChan <- cert2amqp.NewFatalErrorf(nil, "Created new default config file. Please edit %s", filename)
		}
		if runtime.GOOS == "windows" {
			cmd := exec.Command("explorer.exe", "/select,", filename)
			_ = cmd.Start()
		}
		return defaultConfig
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&c)
	c.configfile = filename
	if err != nil {
		errorChan <- cert2amqp.NewFatalErrorf(err, "Error parsing config file %s", filename)
		return defaultConfig
	}
	return c
}

func CreateFileDeleter(deletechan chan string) {
	go func() {
		for filename := range deletechan {
			if Config.RemoveFile {
				err := os.Remove(filename)
				if err != nil {
					errorChan <- cert2amqp.NewNonFatalErrorf(err, "Error deleting certificate file %s: %s", filename, err)
				} else {
					log.Printf("Deleted %s", filename)
				}
			}
		}
	}()
}

func main() {
	// watch input directory for certificate files
	err := cert2amqp.WatchCertDir(Config.Source, errorChan, filenamechan)
	if err != nil {
		errorChan <- cert2amqp.NewFatalErrorf(err, "Error watching %s for new certificates", Config.Source)
		select {}
	}

	// parse certificate files into certificates
	cert2amqp.ProcessCertificates(filenamechan, certChan)

	// delete published files
	CreateFileDeleter(deletechan)

	// create AMQP connection & publish certificates
	cert2amqp.NewAMQPConn(Config.ConnectString, Config.Exchange, Config.Queue, errorChan, certChan, deletechan)
}
