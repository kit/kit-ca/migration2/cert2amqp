module git.scc.kit.edu/KIT-CA/cert2amqp

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/fsnotify/fsnotify v1.6.0
	github.com/josephspurrier/goversioninfo v1.4.0 // indirect
	github.com/k0kubun/colorstring v0.0.0-20150214042306-9440f1994b88 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/rabbitmq/amqp091-go v1.5.0
	golang.org/x/sys v0.3.0 // indirect
)
