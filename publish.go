package cert2amqp

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	_ "github.com/davecgh/go-spew/spew"
	_ "github.com/k0kubun/pp"
	amqp "github.com/rabbitmq/amqp091-go"
)

//const (
//	CONFIRMATIONCHANLEN = 32768
//)

type CertificateExport struct {
	PEM        string `json:"pem"`
	Serial     string `json:"serial"`
	CommonName string `json:"cn"`
}

type AMQPConn struct {
	uri                   string
	exchange              string
	queue                 string
	conn                  *amqp.Connection
	channel               *amqp.Channel
	connectionNotifyClose chan *amqp.Error
	channelNotifyClose    chan *amqp.Error
	//channelPublishConfirmation chan amqp.Confirmation
	certChan   chan *Certificate
	deleteChan chan string
	errorChan  chan error
}

var (
	backoffSeconds = [...]int{1, 2, 4, 8, 16, 32}
)

// Connect to amqp server until a connection is established
func (rc *AMQPConn) connect() {
	for connectionAttempt := 0; ; connectionAttempt++ {
		var delay = 1
		// connect to amqp server
		conn, err := amqp.Dial(rc.uri)

		if err == nil {
			rc.conn = conn
			// get notified on connection problems
			rc.connectionNotifyClose = rc.conn.NotifyClose(make(chan *amqp.Error, 1))

			log.Print("Connection to server established")
			return
		}
		// capped exponential backoff
		if connectionAttempt < len(backoffSeconds) {
			delay = backoffSeconds[connectionAttempt]
		} else {
			delay = backoffSeconds[len(backoffSeconds)-1]
		}
		rc.errorChan <- NewNonFatalError(err, "Unable to connect to AMQP server")
		time.Sleep(time.Duration(delay) * time.Second)
	}
}

// setup amqp connection
func (rc *AMQPConn) setup() {
	var err error

	// create channel
	rc.channel, err = rc.conn.Channel()
	if err != nil {
		rc.errorChan <- NewFatalError(err, "Failed to open a channel")
	}
	// notify on channel close
	rc.channelNotifyClose = rc.channel.NotifyClose(make(chan *amqp.Error, 1))

	// set channel to reliable publishing
	if err = rc.channel.Tx(); err != nil {
		rc.errorChan <- NewFatalError(err, "Unable to put channel into transaction mode")
	}

	// create exchange
	if err = rc.channel.ExchangeDeclare(rc.exchange, "fanout", true, false, false, false, nil); err != nil {
		rc.errorChan <- NewFatalError(err, fmt.Sprintf("Failed to declare exchange %s", rc.exchange))
	}
	// create queue
	var q amqp.Queue
	if q, err = rc.channel.QueueDeclare(rc.queue, true, false, false, false, amqp.Table{"x-queue-type": "quorum"}); err != nil {
		rc.errorChan <- NewFatalError(err, fmt.Sprintf("Failed to declare queue %s", rc.queue))
	}
	// bind queue
	if err = rc.channel.QueueBind(q.Name, "", rc.exchange, false, nil); err != nil {
		rc.errorChan <- NewFatalError(err, fmt.Sprintf("Failed to bind queue %s to exchange %s", q.Name, rc.exchange))
	}
}

// publish certificate
func (rc *AMQPConn) publish(c *Certificate) error {
	// put into JSON structure
	certJSON, err := json.Marshal(CertificateExport{
		PEM:        string(c.PEM),
		Serial:     c.Serial,
		CommonName: c.CommonName,
	})
	// encoding error
	if err != nil {
		return err
	}
	// publish certificate to AMQP
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	err = rc.channel.PublishWithContext(ctx, rc.exchange, "", false, false,
		amqp.Publishing{
			ContentType: "application/json",
			// DeliveryMode: amqp.Persistent,
			Timestamp: time.Now(),
			Body:      certJSON,
			Headers: amqp.Table{
				"CommonName": c.CommonName,
				"Serial":     c.Serial,
				"SHA256":     c.SHA256,
				"NotBefore":  c.NotBefore,
				"NotAfter":   c.NotAfter,
			},
		})
	if err == nil {
		// commit transaction
		if txErr := rc.channel.TxCommit(); txErr != nil {
			return txErr
		}
		log.Printf("Published certificate %s (%s)", c.CommonName, c.Filename)
	}
	return err
}

// NewAMQPConn starts a connection and publish certificates
func NewAMQPConn(uri, exchange, queue string, errorChan chan error, certChan chan *Certificate, deleteChan chan string) {
	var (
		rc = AMQPConn{
			uri:        uri,
			exchange:   exchange,
			queue:      queue,
			errorChan:  errorChan,
			certChan:   certChan,
			deleteChan: deleteChan,
		}
		err error
	)
	// (re-)connect to amqp server
	for {
		log.Print("Connecting to server")

		// connect to amqp server (blocking)
		rc.connect()

		// setup exchanges, queues & notifications
		rc.setup()

		// wait for events
	EVENTS:
		for {
			select {
			// publish new certificate
			case c := <-rc.certChan:
				err = rc.publish(c)
				if err != nil {
					rc.errorChan <- NewNonFatalError(err, "Error publishing certificate")
					break EVENTS
				}
				// queue file for deletion
				deleteChan <- c.Filename
			// connection closed
			case err = <-rc.connectionNotifyClose:
				rc.errorChan <- NewNonFatalError(nil, "Connection closed")
				//rc.channel.Close()
				//rc.conn.Close()
				break EVENTS
			// channel closed
			case err = <-rc.channelNotifyClose:
				rc.errorChan <- NewNonFatalError(nil, "Channel closed")
				//rc.channel.Close()
				break EVENTS
			}
		}
	}
}
