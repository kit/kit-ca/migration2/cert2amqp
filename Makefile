.PHONY: default clean vet
.DEFAULT_GOAL := default

DEPS := cmd/cert2amqp/cert2amqp.go $(wildcard *.go)
winDEPS := res/icon.ico

default: vet cmd/cert2amqp/cert2amqp cmd/cert2amqp/cert2amqp.exe

clean:
	rm -rf cmd/cert2amqp/cert2amqp cmd/cert2amqp/cert2amqp.exe cmd/cert2amqp/resource.syso

vet:
	go vet ./...

res/icon.ico: res/icon.svg
	$(MAKE) -C res $(@F)

cmd/cert2amqp/cert2amqp: $(DEPS)
	$(MAKE) -C cmd/cert2amqp $(@F)

cmd/cert2amqp/cert2amqp.exe: $(DEPS) $(winDEPS)
	$(MAKE) -C cmd/cert2amqp $(@F)

cmd/cert2amqp/cert2amqp_signed.exe: $(DEPS) $(winDEPS)
	$(MAKE) -C cmd/cert2amqp $(@F)
