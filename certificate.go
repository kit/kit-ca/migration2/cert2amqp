package cert2amqp

import (
	"crypto/sha256"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"time"
)

type Certificate struct {
	CommonName string
	PEM        []byte
	Serial     string
	SHA256     string
	Filename   string
	NotBefore  string
	NotAfter   string
}

// ReadCertificates reads alls x509 certificates from a list of input files. Errors are logged and skipped.
func ReadCertificates(filenames ...string) []*x509.Certificate {
	var (
		allcerts []*x509.Certificate
		block    *pem.Block
	)
	for _, filename := range filenames {
		numCerts := len(allcerts)
		// read input file
		content, err := ioutil.ReadFile(filename)
		if err != nil {
			log.Printf("Error reading file %s: %s", filename, err)
			continue
		}
		// try decoding as DER
		certs, err := x509.ParseCertificates(content)
		if err == nil {
			// found certs: append and go to next file
			allcerts = append(allcerts, certs...)
			continue
		}
		// try decoding as PEM
		for block, content = pem.Decode(content); block != nil; block, content = pem.Decode(content) {
			// process only certificates
			if block.Type == "CERTIFICATE" {
				certs, err := x509.ParseCertificates(block.Bytes)
				if err == nil {
					allcerts = append(allcerts, certs...)
				}
			}
		}
		// log unparsable file
		if len(allcerts) == numCerts {
			log.Printf("Unable to parse %s as x509 certificate. Skipping file.", filename)
		}
	}
	return allcerts
}

func ProcessCertificates(infiles chan string, ch chan *Certificate) {
	go func() {
		for incert := range infiles {
			for _, cert := range ReadCertificates(incert) {
				ch <- &Certificate{
					CommonName: cert.Subject.CommonName,
					PEM: pem.EncodeToMemory(&pem.Block{
						Type:  "CERTIFICATE",
						Bytes: cert.Raw,
					}),
					Serial:    cert.SerialNumber.Text(10),
					SHA256:    fmt.Sprintf("%x", sha256.Sum256(cert.Raw)),
					Filename:  incert,
					NotBefore: cert.NotBefore.Format(time.RFC1123Z),
					NotAfter:  cert.NotAfter.Format(time.RFC1123Z),
				}
			}
		}
	}()
}
