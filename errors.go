package cert2amqp

import (
	"fmt"
	"log"
	"os"
	"runtime"
	_ "runtime/debug"
	"time"

	_ "github.com/k0kubun/pp"
)

type FatalError struct {
	originalError error
	description   string
}

func (e FatalError) Error() string {
	if e.originalError == nil {
		return e.description
	} else {
		return e.description + " " + e.originalError.Error()
	}
}

func NewFatalError(err error, description string) FatalError {
	return FatalError{
		originalError: err,
		description:   description,
	}
}

func NewFatalErrorf(err error, format string, a ...interface{}) FatalError {
	return FatalError{
		originalError: err,
		description:   fmt.Sprintf(format, a...),
	}
}

type NonFatalError struct {
	originalError error
	description   string
}

func (e NonFatalError) Error() string {
	if e.originalError == nil {
		return e.description
	} else {
		return e.description + " " + e.originalError.Error()
	}
}

func NewNonFatalError(err error, description string) NonFatalError {
	return NonFatalError{
		originalError: err,
		description:   description,
	}
}

func NewNonFatalErrorf(err error, format string, a ...interface{}) NonFatalError {
	return NonFatalError{
		originalError: err,
		description:   fmt.Sprintf(format, a...),
	}
}

// handle errors (exit on fatal error, log otherwise)
func StartErrorHandler(errorChan chan error) {
	go func() {
		for err := range errorChan {
			switch err.(type) {
			case FatalError:
				log.Printf("[FATAL] %s", err)
				// sleep one minute on windows so people can read the error message on interactive use
				if runtime.GOOS == "windows" {
					time.Sleep(time.Minute)
				}
				os.Exit(1)
			case NonFatalError:
				log.Printf("[ERROR] %s", err)
			default:
				log.Printf("[UNDEF] %s", err)
			}
		}
	}()
}
